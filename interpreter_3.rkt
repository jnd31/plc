#lang racket

;;;; Jack Dessert ;;;;
;;;; Interpreter 3 ;;;

(require "functionParser.rkt")
(define operator car) ;; location of operator in parsed language
(define left-operand cadr) ;; loc of left operand in parsed language
(define right-operand caddr) ;; loc of right operand in parsed language
(define check-right cddr) ;; checking whether right operand exists before parsing
(define variable caaar) ;; loc of variable in the state
(define (package v) (list (box v))) ;; packages the value in a box in a list for storage in the state
(define (var-binding s) (unbox (cadaar s))) ;; delivers the variable binding in the state
(define binding-box cadaar) ;; loc of the box containing the var binding in the state
(define next cdr)
(define this car)
(define true 'true)
(define false 'false)
(define unassigned 'unassigned-variable)
(define op-word caar) ;; loc of operating word in parsed language
(define var-stmt 'var)
(define while-stmt 'while)
(define if-stmt 'if)
(define rtrn 'return)
(define bgn 'begin)
(define brk 'break)
(define ctnue 'continue)
(define thrw 'throw)
(define try 'try)
(define try-body cadr)
(define catch 'catch)
(define (catch-exp lis) (car (car (cdr (cdr lis)))))
(define (catch-check lis) (car (cdr (cdr lis))))
(define (catch-body lis) (car (cdr (cdr (car (cdr (cdr lis)))))))
(define (e lis) (car (car (cdr (car (cdr (cdr lis)))))))
(define finally 'finally)
(define (final-exp lis) (car (car (cdr (cdr (cdr lis))))))
(define (final-check lis) (car (cdr (cdr (cdr lis)))))
(define (final lis) (car (cdr (car (cdr (cdr (cdr lis)))))))
(define empty '())
(define condition cadr) ;; loc of condition in if or while
(define statement caddr) ;; loc of statement in if or while
(define next-if cadddr) ;; go to the next if statement
(define null?-if cdddr) ;; loc to check if the if statements are finished
(define var-loc cadar) ;; loc of variable name in var declaration/assignment
(define value-loc cddar) ;; loc of value in a var declaration/assignment
(define bool-t #t)
(define bool-f #f)
(define sub-stmts cdar) ;; loc of statements in curly braces
(define (push-layer state) (cons empty state)) ;; pushes a layer onto the state
(define pop-layer cdr) ;; removes a layer from the state
(define (next-bind state) (cons (cdar state) (cdr state))) ;; gives the state without the first binding in the top layer
(define this-bind caar) ;; the current binding present in the state
(define (embed a b) (cons a b))
(define (extract s) (car s))
(define func-def 'function)
(define main 'main)
(define func-name cadar)
(define func-param cddar)
(define func-store cddar)
(define func-body cadr)
(define func-params car)
(define funcall 'funcall)
(define funcall-params cddr)
(define funcall-name cadr)
(define final-body cadr)


;; runs the code: parse the file, get the state, return the value
(define interpret
  (lambda (filename)
    (tf2atom (mouter (parser filename)
                     (get-functions (parser filename)
                                    (push-layer empty))))))

;;;; STATE FUNCTIONS ;;;;
;; interprets statements outside of any function bodies
(define mouter
  (lambda (stmts state)
    (cond
      [(null? stmts) (call-func main empty state
                                (lambda (v s)
                                  (error 'undefined "throw called outside a try block")))]
      [(eq? (op-word stmts) func-def) (mouter (next stmts) state)]
      [(eq? (op-word stmts) var-stmt) (mouter (next stmts)
                                              (state-declare-var (var-loc stmts)
                                                                 (value-loc stmts)
                                                                 state
                                                                 (lambda (v s)
                                                                   (error 'undefined "throw called outside a try block"))))]
      [else (error 'undefined "undefined operation in outer statements")])))
    


;; mstate function, takes a list of statements and interprets them
(define mstate
  (lambda (stmts state return break continue throw)
    (cond
      [(null? stmts) state]
      [(eq? (op-word stmts) var-stmt) (mstate (next stmts)
                                              (state-declare-var (var-loc stmts)
                                                                 (value-loc stmts)
                                                                 state throw)
                                              return break continue throw)]
      [(eq? (op-word stmts) '=) (mstate (next stmts)
                                        (massign (left-operand (this stmts))
                                                 (mvalue (right-operand (this stmts))
                                                         state throw)
                                                 state
                                                 (lambda (v) v))
                                        return break continue throw)]
      [(eq? (op-word stmts) rtrn) (return (mvalue (var-loc stmts)
                                                  state throw))]
      [(eq? (op-word stmts) if-stmt) (mstate (next stmts)
                                             (state-handle-if (this stmts)
                                                              state
                                                              return break continue throw)
                                             return break continue throw)]
      [(eq? (op-word stmts) while-stmt) (mstate (next stmts)
                                                (call/cc
                                                 (lambda (k)
                                                   (state-handle-while (this stmts)
                                                                       state
                                                                       return k continue throw)))
                                                return break continue throw)]
      [(eq? (op-word stmts) bgn) (mstate (next stmts)
                                           (pop-layer (mstate (sub-stmts stmts)
                                                              (push-layer state)
                                                              return
                                                              (lambda (v)
                                                                (break (pop-layer v)))
                                                              (lambda (v)
                                                                (continue (pop-layer v)))
                                                              throw))
                                           return break continue throw)]
      [(eq? (op-word stmts) try) (mstate (next stmts)
                                         (state-handle-trycatch (this stmts)
                                                                state
                                                                return break continue throw)
                                         return break continue throw)]
      [(eq? (op-word stmts) brk) (break state)]
      [(eq? (op-word stmts) ctnue) (continue state)]
      [(eq? (op-word stmts) thrw) (throw (mvalue (left-operand (this stmts)) state throw)
                                         state)]
      [(eq? (op-word stmts) func-def) (mstate (next stmts) state
                                              return break continue throw)]
      [(eq? (op-word stmts) funcall) (mstate (next stmts)
                                             (begin (call/cc
                                                     (lambda (k)
                                                       (call-func (func-name stmts)
                                                                  (func-param stmts)
                                                                  state
                                                                  throw)))
                                                    state)
                                             return break continue throw)]
                                                           
      [else (error stmts "undefined expression")])))

;;;; STATE HELPER FUNCTIONS ;;;;
;; assigns a value to the given variable and returns the new state
(define massign
  (lambda (var value state return)
    (cond
      [(and (null? (this state))
            (null? (pop-layer state))) (error var "undeclared variable")]
      [(null? (this state)) (massign var
                                     value
                                     (next state)
                                     (lambda (v)
                                       (return (cons empty
                                                     v))))] 
      [(eq? (variable state) var) (return (begin (set-box! (binding-box state)
                                                           value)
                                                 (state-place-binding (this-bind state)
                                                                      (next-bind state))))]
      [else (massign var
                     value
                     (next-bind state)
                     (lambda (v)
                       (return (state-place-binding (this-bind state)
                                              v))))])))

;; puts the current binding in the state without needing to call the parent function twice
;; helper function for massign
(define state-place-binding
  (lambda (binding next-binding)
    (cons (cons binding
                (this next-binding))
          (next next-binding))))

;;helper function that declares a function and puts it in the state
(define state-declare-func
  (lambda (name body state)
    (cons (cons (append (list name)
                        (package body))
                (this state))
          (pop-layer state))))

;; helper function for when the state to assign into and the state to lookup the value from are the same state
;; basically for when the value to be returned is a variable and not a function evaluation
(define state-declare-var
  (lambda (var value state throw)
    (state-declare-var-funcs var value state state throw)))

;; helper function that declares a variable and puts it in the state, checking for values
;; in the lookup state rather than the assign state
(define state-declare-var-funcs
  (lambda (var value state lookup-state throw)
    (if (null? value)
        (cons (cons (append (list var)
                            (package unassigned))
                    (this state))
              (pop-layer state))
        (cons (cons (append (list var)
                            (package (mvalue (this value)
                                             lookup-state
                                             throw)))
                    (this state))
              (pop-layer state)))))

;; handles an if statement
(define state-handle-if
  (lambda (stmt state return break continue throw)
    (cond
      [(null? stmt) state]
      [(mboolean (condition stmt) state throw) (mstate (list (statement stmt))
                                                 state
                                                 return break continue throw)]
      [(null? (null?-if stmt)) state]
      [else (mstate (list (next-if stmt)) state return break continue throw)])))

;; handles a while statement
(define state-handle-while
  (lambda (stmt state return break continue throw)
    (cond
      [(null? stmt) state]
      [(mboolean (condition stmt) state throw) (state-handle-while stmt
                                                             (call/cc
                                                              (lambda (c)
                                                                (mstate (list (statement stmt))
                                                                        state
                                                                        return break c throw)))
                                                             return break continue throw)]
      [else state])))

;; makes a continuation for the catch block
(define create-throw-catch-continuation
  (lambda (stmt state return break continue throw jump finally-block)
    (cond
      [(null? (catch-check stmt)) (lambda (ex state) (throw ex (mstate finally-block
                                                                       state
                                                                       return break continue throw)))]
      [(not (eq? catch (car (catch-check stmt)))) (error (catch-check stmt) "incorrect catch statement")]
      [else (lambda (ex s)
              (jump (mstate finally-block
                            (pop-layer (mstate (catch-body stmt)
                                               (state-declare-var (e stmt)
                                                                  (list ex)
                                                                  (push-layer s)
                                                                  throw)
                                               return
                                               (lambda (s) (break (pop-layer s)))
                                               (lambda (s) (continue (pop-layer s)))
                                               (lambda (ex s) (throw ex (pop-layer s)))))
                            return break continue throw)))])))

;; handles a try-catch block
(define state-handle-trycatch
  (lambda (stmt state return break continue throw)
    (call/cc
     (lambda (jump)
       (mstate (make-finally-block (final-check stmt))
               (mstate (make-try-block stmt)
                       state
                       (lambda (v)
                         (begin (mstate (make-finally-block (final-check stmt))
                                        state return break continue throw)
                                (return v)))
                       (lambda (s)
                         (break (mstate (make-finally-block (final-check stmt))
                                        state return break continue throw)))
                       (lambda (s)
                         (continue (mstate (make-finally-block (final-check stmt))
                                           state return break continue throw)))
                       (create-throw-catch-continuation stmt state
                                                        return break continue throw
                                                        jump (make-finally-block (final-check stmt))))
               return break continue throw)))))

;; makes a try statement into a begin block
(define make-try-block
  (lambda (stmt)
    (list (cons bgn (try-body stmt)))))

;; makes a finally statement into a begin block
(define make-finally-block
  (lambda (stmt)
    (cond
      [(null? stmt) (list (list bgn))]
      [else (list (cons bgn (final-body stmt)))])))

;; goes through the given statements and returns a state that has all in-scope functions declared
(define get-functions
  (lambda (stmts state)
    (cond
      [(null? stmts) state]
      [(eq? (op-word stmts) func-def) (get-functions (next stmts)
                                                     (state-declare-func (func-name stmts)
                                                                         (func-store stmts)
                                                                         state))]
      [else (get-functions (next stmts) state)])))





;;;; VALUE SECTION ;;;;
;a function that can take an expression of numbers and operators and return the value
(define mvalue
  (lambda (exp state throw)
    (cond
      [(null? exp) (error exp "undefined expression")]
      [(number? exp) exp]
      [(or (eq? true exp)
           (eq? false exp)) (atom2tf exp)]
      [(not (list? exp)) (value-lookup exp
                                       state)]
      [(eq? '+ (operator exp)) (+ (mvalue (left-operand exp)
                                          state throw)
                                  (mvalue (right-operand exp)
                                          state throw))]
      [(and (eq? '- (operator exp))
            (null? (check-right exp))) (* -1 (mvalue (left-operand exp)
                                                     state throw))]
      [(eq? '- (operator exp)) (- (mvalue (left-operand exp)
                                          state throw)
                                  (mvalue (right-operand exp)
                                          state throw))]
      [(eq? '* (operator exp)) (* (mvalue (left-operand exp)
                                          state throw)
                                  (mvalue (right-operand exp)
                                          state throw))]
      [(eq? '/ (operator exp)) (quotient (mvalue (left-operand exp)
                                                 state throw)
                                         (mvalue (right-operand exp)
                                                 state throw))]
      [(eq? '% (operator exp)) (remainder (mvalue (left-operand exp)
                                                  state throw)
                                          (mvalue (right-operand exp)
                                                  state throw))]
      [(eq? funcall (operator exp)) (call-func (funcall-name exp) (funcall-params exp) state throw)]
      [else (mboolean exp state throw)]))) ;; because an assignment can be a boolean

;;;; HELPER FUNCTIONS FOR MVALUE ;;;;

;; helper function that finds a variable in the state and returns its value
(define value-lookup
  (lambda (var state)
    (cond
      [(and (null? (this state))
            (null? (pop-layer state))) (error var "undefined variable")]
      [(null? (this state)) (value-lookup var
                                          (pop-layer state))]
      [(and (eq? (variable state) var)
            (eq? (var-binding state) unassigned) (error 'undefined "unassigned variable"))]      
      [(eq? (variable state) var) (var-binding state)]
      [else (value-lookup var
                          (next-bind state))])))

;; helper function that calls a function--also used in mstate but less often
(define call-func
  (lambda (funcname params state throw)
    (call/cc
     (lambda (k)
       (mstate (func-body (value-lookup funcname state))
               (state-assign-params (func-params (value-lookup funcname state))
                                    params
                                    (get-functions (func-body (value-lookup funcname
                                                                            state))
                                                   (push-layer (get-fn-scope funcname
                                                                             state
                                                                             (lambda (v) v))))
                                    state throw)
               k
               (lambda (v)
                 (error 'undefined "break called outside a loop"))
               (lambda (v)
                 (error 'undefined "continue called outside a loop"))
               (lambda (ex s) (throw ex state)))))))

;; helper function for call-func that returns the state that has the layer with the given function defined in it and no more
(define get-fn-scope
  (lambda (funcname state return)
    (cond
      [(and (null? (this state))
            (null? (pop-layer state))) (error funcname "undefined function")]
      [(null? (this state)) (get-fn-scope funcname
                                          (pop-layer state)
                                          (lambda (v) v))]
      [(eq? (variable state) funcname) (return state)]
      [else (get-fn-scope funcname
                          (next-bind state)
                          (lambda (v)
                            (return (state-place-binding (this-bind state)
                                                         v))))])))
                          
;; helper function that takes a list of parameters and a list of expressions for their values
;; and assigns them in the state--used to assign parameters for functions
(define state-assign-params
  (lambda (p-names p-values state lookup-state throw)
    (cond
      [(and (null? p-names) (null? p-values)) state]
      [(null? p-names) (error "more parameters given than defined in function")]
      [(null? p-values) (error "not enough parameters given for the function")]
      [else (state-assign-params (next p-names)
                                 (next p-values)
                                 (state-declare-var-funcs (this p-names)
                                                          (list (this p-values))
                                                          state lookup-state
                                                          throw)
                                 lookup-state throw)])))




;;;; BOOLEAN SECTION ;;;;
;; takes an expression and returns whether it is true or false
(define mboolean
  (lambda (exp state throw)
    (cond
      [(null? exp) (error exp "undefined expression")]
      [(or (eq? true exp)
           (eq? false exp)) (atom2tf exp)]
      [(not (list? exp)) (value-lookup exp
                                       state)]
      [(eq? '== (operator exp)) (equal? (mvalue (left-operand exp)
                                                state throw)
                                        (mvalue (right-operand exp)
                                                state throw))]
      [(eq? '!= (operator exp)) (not (equal? (mvalue (left-operand exp)
                                                     state throw)
                                             (mvalue (right-operand exp)
                                                     state throw)))]
      [(eq? '< (operator exp)) (< (mvalue (left-operand exp)
                                          state throw)
                                  (mvalue (right-operand exp)
                                          state throw))]
      [(eq? '> (operator exp)) (> (mvalue (left-operand exp)
                                          state throw)
                                  (mvalue (right-operand exp)
                                          state throw))]
      [(eq? '<= (operator exp)) (<= (mvalue (left-operand exp)
                                            state throw)
                                    (mvalue (right-operand exp)
                                            state))]
      [(eq? '>= (operator exp)) (>= (mvalue (left-operand exp)
                                            state throw)
                                    (mvalue (right-operand exp)
                                            state throw))]
      [(eq? '&& (operator exp)) (and (mvalue (left-operand exp)
                                               state throw)
                                     (mvalue (right-operand exp)
                                               state throw))]
      [(eq? '|| (operator exp)) (or (mvalue (left-operand exp)
                                              state throw)
                                    (mvalue (right-operand exp)
                                              state throw))]
      [(eq? '! (operator exp)) (not (mvalue (left-operand exp)
                                              state throw))])))


;; modifies #t and #f into 'true and 'false, respectively, for use in returning interpret
(define tf2atom
  (lambda (var)
    (cond
      [(eq? var bool-t) true]
      [(eq? var bool-f) false]
      [else var])))

;; helper for mboolean, modifies 'true and 'false into #t and #f, for use in interpreting into booleans
(define atom2tf
  (lambda (var)
    (cond
      [(eq? true var) bool-t]
      [(eq? false var) bool-f]
      [else var])))
